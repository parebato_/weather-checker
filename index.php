<?php 

//include 'script.php';

$weather = "";
$error = "";
$city = "";

//input name
if(isset($_GET['city'])){

//replace spaces for a valid string
		$city = str_replace(" ", '', $_GET['city']);

//check if the page exists or not
		$file_headers = @get_headers("https://www.weather-forecast.com/locations/".$city."/forecasts/latest");

if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {

		$error = " is an invalid city name. Please check your spelling.";
} else{
    
    $resultPage = file_get_contents("https://www.weather-forecast.com/locations/".$city."/forecasts/latest");
    
    // explode array  //create a new variable  // get the string the website
   

    $arrayOne = explode('<span class="b-forecast__table-description-title">', $resultPage);
	
    $weather = $arrayOne[1];

	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Weather Checker</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<!-- Font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat+Alternates:600&display=swap" rel="stylesheet">
	<!--	custom style -->
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<div class="bg">

		<div class="container">
			<h1>Weather Checker</h1>
			<form class="form-group">
				<label for="city">Choose city name</label>
				<input id="city" name="city" class="form-control" type="text">

				<button class="btn my-4" type="submit">Check the Weather</button>
			</form>

			<hr>

			<div class="weather">
				<?php 
					if(empty($_GET['city'])){
						echo "Enter valid city name";
					}else {
						echo $weather;
					}
				 ?>
			</div>
		</div>
	</div>

</body>

</html>
